import shutil
import os
#Clean the build directory
shutil.rmtree("Temp")

#Grab the latest version of the repo
os.system("git pull origin")

#Build all targets
SolutionName = os.getcwd() + "\\Sandkit.sln"
SolnConfigNames = ["Debug","Profile","Release"]
for SolnConfigName in SolnConfigNames:
	os.system("Devenv \"" + SolutionName + "\" /build " + SolnConfigName)