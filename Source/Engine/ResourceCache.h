#pragma once

//Standard library includes
#include<iostream>
#include<fstream>
#include<iterator>
#include<memory>
#include<string>
#include<map>

class AppLayer;

//Standard library
#include <ctime>

//Custom includes
#include"AppLayer.h"

/* The resource cache provides an interface to speed up the usage of frequently used files.
   The most recently used files are kept in memory until the cache is filled up, after which
   files are removed to make space for the new file. */
class ResourceCache
{
protected:
	typedef std::vector<char> FileContent;

	struct FileEntry
	{
		std::time_t timeStamp = 0;		                                //Last time the file was requested
		std::shared_ptr< FileContent > fileContentPtr = nullptr;        //Pointer to file contents
		std::weak_ptr< FileContent > fileContentWeakPtr;                //Weak pointer to file contents
	};

	ResourceCache();
	virtual ~ResourceCache();

	static std::map<std::wstring, FileEntry> m_fileMap;

	static AppLayer* mp_appLayer;
//	static bytes_count_t m_maxSize;
//	static bytes_count_t m_usedSpace;

	static void attachToApplication(AppLayer& _appLayer);

public:
	/* Takes a file path or template relative to the current working directory. 
	   Forces precaching the files that match the template or filename.
	   On success: Returns a shared pointer to a vector that can be used to
	               randomly access the last file successfully allocated by the call.
	   On failure: Returns empty pointer, cache is not affected. */
	static std::shared_ptr< const FileContent > PreloadFile(const std::wstring& _relativeDir);

	/* Takes a file path relative to the current working directory.
	Checks if the file is already cached, and caches the file if it isn't.
	On success: Returns shared pointer to vector containing raw file contents, file is cached.
	on failure: Returns empty pointer. */
	static std::shared_ptr< const FileContent > GetFileContents(const std::wstring& _relativeDir);

	/* Takes a file path and deletes the cached contents of that file.
	   May not delete the file from cache immediately, but rather wait
	   until all instances of it are closed. */
	static void RemoveFileFromCache(const std::wstring& _fileName);

	friend class AppLayer;
};