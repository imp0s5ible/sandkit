#include "AppLayer.h"

#ifdef _WIN32
	#include "AppLayerWin.cpp"
#else
	#error No application layer found for this platform!
#endif