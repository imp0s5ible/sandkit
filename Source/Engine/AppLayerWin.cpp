#pragma once
#ifdef _WIN32

//Standard Library
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <limits>

//Win32 API
#include <Windows.h>
#include <fileapi.h>
#include <WinBase.h>
#include <winreg.h>
#undef max //Avoid conflict between minwindef.h and limits.h in STL

//Custom includes
#include "AppLayer.h"

//Custom constants
const size_t workingDirMaxSize = 200;
const bytes_count_t GIGABYTE = 1024 * 1024 * 1024; //One gigabtye, in bytes
const bytes_count_t freeMemoryNeeded = 1073741824; //2 GB

AppLayer::AppLayer()
{

}

AppLayer::~AppLayer()
{

}

void AppLayer::precacheResources()
{
	ResourceCache::attachToApplication(*this);
	ResourceCache::PreloadFile(L"Assets\\Pictures\\*");
}

bool AppLayer::init()
{
	//Find out working directory
	wchar_t workingDirBuf[workingDirMaxSize + 1];
	GetCurrentDirectory(workingDirMaxSize + 1, workingDirBuf);

	m_directoryVector[L"WORKING_DIR"] = std::wstring(workingDirBuf);

	//Load display string resources
	loadStringResource();

	if (!isOnlyInstance())
	{
		MessageBox(NULL, static_cast<LPCWSTR>( &( getString(L"MULTI_INSTANCE_ERROR")[0] ) ),
			static_cast<LPCWSTR>( &( getString(L"GAME_TITLE")[0] ) ),
			MB_OK | MB_ICONWARNING);
		return false;
	}

	//Take inventory of CPU speed
	m_cpuSpeed = readCPUSpeed();
	std::wcout << "CPU speed: " << m_cpuSpeed << " cycles per second (" << std::setprecision(3) << m_cpuSpeed / 1000.0 << "Ghz)" << std::endl;

	m_storageSpace = checkFreeStorageSpace(m_directoryVector[L"WORKING_DIR"]);
	std::wcout << "Working directory: " << m_directoryVector[L"WORKING_DIR"] << std::endl;
	std::wcout << "Available disk space: " << std::setprecision(3) << m_storageSpace/static_cast<double>(GIGABYTE) << "GB" << std::endl;

	//Take inventory of memory
	m_physicalMemory = checkPhysicalMemory();
	std::wcout << "Total physical memory: " << std::setprecision(3) << m_physicalMemory/static_cast<double>( GIGABYTE ) << "GB" << std::endl;
	std::wcout << "Available memory: " << std::setprecision(3) << checkFreeMemory()/static_cast<double>(GIGABYTE) << "GB" << std::endl;
	if (checkFreeMemory() < freeMemoryNeeded)
	{
		std::wcerr << "Not enough free memory to run program!" << std::endl;
		return false;
	}

	//Make sure we have enough contiguous memory, make sure all that memory is accessible too
	unsigned char *testBuf = static_cast<unsigned char *>( malloc(freeMemoryNeeded) );
	if (testBuf == nullptr)
	{
		std::wcerr << "Not enough contiguous memory to run program!" << std::endl;
		return false;
	}
	memset(testBuf, 0, freeMemoryNeeded);
	free(testBuf);

	precacheResources();
	preloadResources();

	initScriptManager();
	initEventManager();

	//String test
	std::wcout << L"String test:" << std::endl;
	std::wcout << L"GAME_TITLE: " << getString(L"GAME_TITLE") << L'\n';
	std::wcout << L"PLAYER_NAME: " << getString(L"PLAYER_NAME") << L'\n';
	std::wcout << L"MULTI_LINE_TEST:\n" << getString(L"MULTI_LINE_TEST") << L'\n';
	std::wcout << L"WELCOME_MSG_TEST: " << getString(L"WELCOME_MSG_TEST") << L'\n';
	std::wcout << L"EMPTY_STRING: " << getString(L"EMPTY_STRING") << L'\n';
	std::wcout << L"NONEXISTENT_STRING: " << getString(L"NONEXISTENT_STRING") << L'\n';
	std::wcout << getString(L"MESSED_UP") << L"END\n";
	std::wcout << std::flush;

	//TODO - Initialize game logic
	//TODO - Initialize game views
	return true;
}

bytes_count_t AppLayer::checkFreeMemory() const
{
	MEMORYSTATUSEX memStat;
	GlobalMemoryStatusEx(&memStat);
	return static_cast<bytes_count_t>(memStat.ullAvailVirtual);
}

bytes_count_t AppLayer::checkPhysicalMemory() const
{
	MEMORYSTATUSEX memStat;
	memStat.dwLength = sizeof(memStat);
	GlobalMemoryStatusEx(&memStat);

	return static_cast<bytes_count_t>(memStat.ullTotalPhys);
}

bytes_count_t AppLayer::checkFreeStorageSpace(const std::wstring& dirName) const
{
	bytes_count_t numFreeBytes;
	GetDiskFreeSpaceEx(dirName.c_str(),
		reinterpret_cast<PULARGE_INTEGER>( &numFreeBytes ),
		NULL, NULL);
	return static_cast<bytes_count_t>( numFreeBytes );
}

unsigned long AppLayer::readCPUSpeed() const
{
	DWORD mhz = 0;
	DWORD bufSize = sizeof(DWORD);
	DWORD type = REG_DWORD;
	HKEY registryKey;

	long error = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
		L"HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0",
		0, KEY_READ, &registryKey);

	if (error == ERROR_SUCCESS)
	{
		RegQueryValueEx(registryKey, L"~MHz", NULL, &type, reinterpret_cast<LPBYTE>( &mhz ), &bufSize);
	}
	return static_cast<unsigned long>(mhz);
}

void AppLayer::loadStringResource()
{
	std::vector< std::wstring > stringFileList = findAllFilesWithName(L"Assets\\DisplayStrings\\*");
	for (auto fileIter = stringFileList.cbegin(); fileIter != stringFileList.cend(); ++fileIter)
	{
		std::wifstream inStream(L"Assets\\DisplayStrings\\" + *fileIter);
		if (inStream.good())
		{
			TextParser::parseStringFileIntoMap(m_displayStringVector, inStream);
		}
		else
		{
			std::wcerr << "Warning: Error opening string resource file " << (L"Assets\\DisplayStrings\\" + *fileIter) << std::endl;
		}
	} //End for
}

void AppLayer::initScriptManager()
{
}

void AppLayer::initEventManager()
{
}

void AppLayer::loadGameOptions()
{
}

void AppLayer::initRenderer()
{
}

void AppLayer::initGameLogic()
{
}

void AppLayer::setDirectories()
{
}

void AppLayer::preloadResources()
{
}

bool AppLayer::isOnlyInstance() const
{
	//Attempt to create a mutex with our title. If this fails, another copy of this program is already open!
	CreateMutex(NULL, TRUE, getString(L"GAME_TITLE").c_str());
	return (GetLastError() == ERROR_SUCCESS);
}

std::vector<std::wstring> AppLayer::findAllFilesWithName(const std::wstring& _fileName) const
{
	std::vector<std::wstring> fileNames;

	WIN32_FIND_DATA fileData;
	HANDLE fileHandle;

	//WIN32 API call - finds the first file that matches the expression _fileName
	fileHandle = FindFirstFile(_fileName.c_str(), &fileData);
	if (fileHandle != INVALID_HANDLE_VALUE)
	{
		do
		{
			std::wstring currentFileName = fileData.cFileName;
			//Make sure the files we're reading are not the this directory/up directory symlinks
			if (currentFileName != L".." && currentFileName != L".")
			{
				fileNames.push_back(currentFileName);
			}
		} while (FindNextFile(fileHandle,&fileData));
	}
	return fileNames;
}

const std::wstring& AppLayer::getDirectory(const std::wstring& _fileName) const
{
	return (*m_directoryVector.find(_fileName)).second;
}

const std::wstring& AppLayer::getString(const std::wstring& _stringID) const
{
	auto foundStringPos = m_displayStringVector.find(_stringID);
	if (foundStringPos != m_displayStringVector.end())
	{
		return (*foundStringPos).second;
	}
	else
	{
		return _stringID;
	}
}

AppLayer& AppLayer::instance()
{
	static AppLayer inst;
	return inst;
}

#endif // _WIN32 or _WIN64