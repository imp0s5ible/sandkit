#include<iostream>

#include"AppLayer.h"
#include"ObjectPool.h"

int main(int argc, char* argv[])
{
	ObjectPool<int>::addPoolBlock(10);
	int* p_test = ObjectPool<int>::Allocate();
	*p_test = 0xDEADBEEF;
	int* p_test2 = ObjectPool<int>::Allocate();
	*p_test2 = 0xB00B1E5;

	bool initSuccess = AppLayer::instance().init();
	std::wcout << (initSuccess ? L"Initialization successful!" : L"Initialization failed!") << std::endl;
	std::wcout << "Pool allocated memory values:" << std::hex << *p_test << " " << *p_test2 << std::endl;
	ObjectPool<int>::Free(p_test);
	ObjectPool<int>::Free(p_test2);
	std::wcin.get();
	return 0;
}