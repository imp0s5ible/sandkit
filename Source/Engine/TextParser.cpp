#include "TextParser.h"

//STL includes
#include <string>
#include <streambuf>
#include <vector>
#include <algorithm>
#include <iostream>

TextParser::TextParser()
{
}


TextParser::~TextParser()
{
}

StringMap& TextParser::parseStringFileIntoMap( std::map<std::wstring, std::wstring>& _stringMap, std::wifstream & _inStream)
{
    //Detect file size and reserve enough memory in the temporary string
	_inStream.seekg(0, _inStream.end);
	size_t fileSize = static_cast<size_t>( _inStream.tellg() );
	_inStream.clear();
	_inStream.seekg(0);

	std::wstring fileContents;
	fileContents.reserve(fileSize);
	fileContents.assign(std::istreambuf_iterator<wchar_t>(_inStream),
		std::istreambuf_iterator<wchar_t>());


	//Parse the cached file and record each string into the database
	auto substringStart = fileContents.cbegin();
	for (auto substringEnd = substringStart;substringEnd != fileContents.cend(); ++substringEnd)
	{
		//Scan the buffer for the string ID of our current entry
		substringStart = std::find_if_not(substringEnd, fileContents.cend(), iswspace);
		substringEnd = std::find(substringStart, fileContents.cend(), L':');

		//Empty string ID: This happens if we had a colon with no proper ID in front of it
		if (substringStart == substringEnd)
		{
			std::wcerr << "Fatal: Encountered empty string ID! Aborting..." << std::endl;
			break; //Abort parsing, we don't want to read bad info from the rest of the (now broken) file.
		}
		std::wstring stringID = std::wstring(substringStart, substringEnd);
	    if (substringEnd == fileContents.cend() || substringEnd + 1 == fileContents.cend())
		{
			std::wcerr << "Warning: Last string ID \"" << stringID << "\" was read, but no string contents provided!" << std::endl;
			break;
		}

		//Seek to just after the first quotation mark, marking the beginning of string contents
		do
		{
			++substringEnd;
		}while (*(substringEnd - 1) != '"' && substringEnd != fileContents.cend());

		//String content range starts as empty, then expands to the next quotation mark
		substringStart = substringEnd;
		substringEnd = std::find(substringStart, fileContents.cend(), L'"');

		std::wstring stringContent = std::wstring(substringStart, substringEnd);
		_stringMap[stringID] = stringContent;

		if (substringEnd == fileContents.cend())
		{
			std::wcerr << "Warning: Last string \"" << stringID << "\" was recorded without a terminating quotation mark!" << std::endl;
			break;
		}
		//Otherwise substringEnd rests on the final quotation mark of this entry, and will be incremented in the next turn
	}
	return _stringMap;
}

StringMap& TextParser::parseStringFileIntoMap(std::map< std::wstring, std::wstring >& _stringMap, std::wstring & _fileName)
{
	return parseStringFileIntoMap(_stringMap, std::wifstream(_fileName, std::ios::in));
}