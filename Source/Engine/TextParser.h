#pragma once

//STL includes
#include <map>
#include <fstream>

typedef std::map<std::wstring, std::wstring> StringMap;

class TextParser
{
public:

	/*Parses our custom string format into the given string map.*/
	static StringMap& parseStringFileIntoMap(std::map<std::wstring, std::wstring>& _stringMap, std::wifstream& _inStream);
	static StringMap& parseStringFileIntoMap(std::map<std::wstring, std::wstring>& _stringMap, std::wstring& _fileName);
	
private:
	TextParser();
	virtual ~TextParser();
};

