#include "ResourceCache.h"
#include<cassert>

//Static member declarations
AppLayer* ResourceCache::mp_appLayer = nullptr;

std::map<std::wstring, ResourceCache::FileEntry> ResourceCache::m_fileMap;

//bytes_count_t ResourceCache::m_maxSize = 1024 * 1024 * 512; //512 Mb
//bytes_count_t ResourceCache::m_usedSpace = 0;

//Constructors/destructors
ResourceCache::ResourceCache(){}

ResourceCache::~ResourceCache(){}

//Settings
void ResourceCache::attachToApplication(AppLayer& _appLayer)
{
	mp_appLayer = &_appLayer;
}

std::shared_ptr< const ResourceCache::FileContent > ResourceCache::PreloadFile(const std::wstring& _fileName)
{
	//Ask the application layer for a list of all files that match the string in _fileName
	std::vector<std::wstring> filesFound = mp_appLayer->findAllFilesWithName(mp_appLayer->getDirectory(L"WORKING_DIR") + L"\\" + _fileName);

	size_t last_slash = _fileName.find_last_of(L"/\\");
	std::wstring directory = _fileName.substr(0, last_slash+1);

	FileEntry currentEntry;
	for (auto fileIter = filesFound.cbegin();fileIter != filesFound.cend(); ++fileIter)
	{
		std::wstring currentFilePath = directory + *fileIter;
		std::wcout << L"Precaching file " << currentFilePath << std::flush;

		//Attempt to open the input file in binary mode
		std::ifstream inStream(currentFilePath,
			std::ios::binary | std::ios::ate | std::ios::in);  //Open the file at the end so we can instantly tell the filesize
		inStream >> std::noskipws;
		if (!inStream.good())
		{
			std::wcout << L" ...error opening file!" << std::endl;
			continue;
		}

		unsigned fileSize = static_cast<unsigned>( inStream.tellg() );
		std::wcout << std::endl << L"\t" << fileSize << std::flush;

		//Clear and reset the buffer so we can start copying into memory
		inStream.clear();
		inStream.seekg(0);

		//Allocate space for whole file and copy the file into it at once
		currentEntry.fileContentPtr = std::shared_ptr< FileContent >( new FileContent(fileSize) );
		inStream.read(&(*currentEntry.fileContentPtr)[0], fileSize);						//Faster than std::vector::assign or some similar iterator based approach. Like, a lot faster.
		currentEntry.fileContentWeakPtr = currentEntry.fileContentPtr;
		std::wcout << L"\\" << currentEntry.fileContentPtr->size() << " bytes read:" << std::flush;

		currentEntry.timeStamp = std::time(nullptr);
		m_fileMap[currentFilePath] = currentEntry;

		std::wcout << L" OK!" << std::endl;
		//m_usedSpace += fileSize;
	}
	return currentEntry.fileContentPtr;
}

void ResourceCache::RemoveFileFromCache(const std::wstring& _fileName)
{
	auto fileLocationInMap = m_fileMap.find(_fileName);

	//If the file entry exists in cache
	if (fileLocationInMap != m_fileMap.cend())
	{
		//We remove the entry's ownership of the file contents
		(*fileLocationInMap).second.fileContentPtr = nullptr;
		//If the above release caused the deallocation of the file contents (becuase the file wasn't open anywhere else)
		if ((*fileLocationInMap).second.fileContentWeakPtr.use_count() == 0)
		{
			//Then remove the entry for the file entirely
			m_fileMap.erase(fileLocationInMap);
		}
		/* Otherwise we keep the entry for the (still existing) file contents around
		   so we won't have to deal with reallocating it if a new request comes in the meantime */
	}
}

//Queries

std::shared_ptr< const ResourceCache::FileContent > ResourceCache::GetFileContents(const std::wstring& _fileName)
{
	auto fileLocationInMap = m_fileMap.find(_fileName);

	if (fileLocationInMap == m_fileMap.cend())
	{
		return PreloadFile(_fileName);
	}
	else
	{
		FileEntry& foundFileEntry = (*fileLocationInMap).second;
		foundFileEntry.fileContentPtr = foundFileEntry.fileContentWeakPtr.lock();
		if (foundFileEntry.fileContentPtr)
		{
			foundFileEntry.timeStamp = std::time(nullptr);
			return foundFileEntry.fileContentPtr;
		}
		else
		{
			return PreloadFile(_fileName);
		}
	}
}