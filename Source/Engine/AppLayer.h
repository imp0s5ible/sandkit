#pragma once

//STL includes
#include<cstdint>
#include<string>
#include<vector>
#include<map>

class ResourceCache;
//Custom includes
#include "ResourceCache.h"
#include "TextParser.h"



//bytes_count_t is a type capable of storing data about the amount of bytes (such as the amount of free space on a hard disk, in bytes)
typedef uint64_t bytes_count_t;

class AppLayer
{
//Fields
protected:
	union Option
	{
		long int m_integer;
		double m_float;
		wchar_t* m_string;
		bool m_boolean;
	};

	//Number of CPU clock cycles per second
	size_t m_cpuSpeed;

	//Size of physical memory, in bytes
	bytes_count_t m_physicalMemory;

	//Secondary storage space
	bytes_count_t m_storageSpace;

	//Resource of strings that are displayed to a viewer
	std::map<std::wstring, std::wstring> m_displayStringVector;

	//Resource of directories
	std::map<std::wstring, std::wstring> m_directoryVector;

	//Resource of options
	std::map<std::wstring, Option> m_optionVector;

	
//Public methods
public:
	/*Returns the singleton instance of the application layer.*/
	static AppLayer& instance();

	//Initiates the singleton instance of the application layer.
	//Returns false if there has been a critical failure
	//Remember to call destroyInstance() when quitting the application!
	bool init();

	//Returns the amount of available memory, in bytes
	bytes_count_t checkFreeMemory() const;

	//Returns the total physical memory, in bytes
	bytes_count_t checkPhysicalMemory() const;

	//Returns the amount of hard disk storage space, in bytes
	bytes_count_t checkFreeStorageSpace(const std::wstring&) const;

	//Returns the number of CPU clock cycles per second
	unsigned long readCPUSpeed() const;

	//Returns a text resource, based on its identifier
	const std::wstring& getString(std::wstring&) const;

protected:
	//Constructor
	AppLayer();

	//Destructor
	virtual ~AppLayer();

//Private methods
private:
// --- Resource loading functions --- //

	//Load the game's resource cache
	void precacheResources();

	//Load string resources, such as strings displayed in the UI, according to localization
	void loadStringResource();

	//Sets directories for file operations, such as save games and temporary files
	void setDirectories();

	//Preload selected resources from the resource cache
	void preloadResources();

// --- Initialization functions --- //

	//Create the engine's script manager
	void initScriptManager();

	//Create the engine's event manager
	void initEventManager();

	//Load initialg game options and configurations
	void loadGameOptions();

	//Initialize renderer, creates application window
	void initRenderer();

	//Create game logic and game views
	void initGameLogic();

// --- Query functions --- //

	//Returns true if this is the only running instance of the engine
	bool isOnlyInstance() const;

	//Returns the directory with the proper identifier
	const std::wstring& getDirectory(const std::wstring& _dirID) const;

	/* Returns the display string with the proper identifier
	   If the given string is not found, the identifier itself is returned,
	   it is therefore recommended to choose string IDs that can be easily
	   identified in text displayed in-game. */
	const std::wstring& getString(const std::wstring& _stringID) const;

	//Returns a list of fully qualified file names using a path template
	std::vector<std::wstring> findAllFilesWithName(const std::wstring& _fileName) const;

	friend class ResourceCache;
};

