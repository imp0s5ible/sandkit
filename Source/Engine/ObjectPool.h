#pragma once

#include <cassert>
#include <stdlib.h>
#include <vector>

template <typename ObjectType>
class ObjectPool
{
private:
	//When not in use, a slot for a given object points to the next available free slot
	union PoolItem
	{
		ObjectType  m_object;
		PoolItem* m_next;
	};

	//An entry for a pool block contains information about its size, and a pointer to the beginning of the block's contents.
	struct PoolBlock
	{
		size_t m_blockSize;
		PoolItem* m_poolContent;
	};

	static std::vector<PoolBlock> m_blockVector;
	static PoolItem* m_nextAvailable;
	static size_t m_autoExpand; //Size of block to automatically allocate if pool is full. Zero means no automatic expansion.
	static ObjectType m_prototype;

	/* Returns the number of unused slots belonging to this particular
	   pool block. May be expensive! */
	size_t getNumberOfFreeSlots(const PoolBlock& _block) const
	{

	}

public:
	/* Sets the amount of objects the pool should expand by if it's full. 
	   Setting this to zero disables automatic pool expansion. */
	static void setAutoExpand(const size_t& _newAutoExpand)
	{
		m_autoExpand = _newAutoExpand;
	}

	/* Adds a pool block to the pool capable of holding _objectCount objects.
	   Existing pool blocks are unaffected. */
	static void addPoolBlock(size_t _objectCount)
	{
		//Push a new block information entry into our block vector
		PoolBlock newBlock;

		newBlock.m_poolContent = static_cast<PoolItem *>( malloc(_objectCount * sizeof(PoolItem)) );
		newBlock.m_blockSize = _objectCount;

		if (newBlock.m_poolContent == nullptr)
		{
			return; //TODO: Handle allocation error case
		}

		//If all our pools are full
		if (m_nextAvailable == nullptr)
		{
			//The list of available blocks starts with the first slot in the new pool block
			m_nextAvailable = &(newBlock.m_poolContent[0]);
		}
		else
		{
			//Link the block to the existing linked list of free slots
			PoolItem* itemPtr = m_nextAvailable;
			while (itemPtr->m_next != nullptr)
			{
				itemPtr = itemPtr->m_next;
			}
			itemPtr->m_next = &(newBlock.m_poolContent[0]);
		}

		//Set up linked list for the newly allocated block
		for (size_t itemInd = 0; itemInd < _objectCount - 1; ++itemInd)
		{
			newBlock.m_poolContent[itemInd].m_next = &(newBlock.m_poolContent[itemInd + 1]);
		}
		newBlock.m_poolContent[_objectCount - 1].m_next = nullptr;

		m_blockVector.push_back(newBlock);
	}

	/*Allocates memory for an object of this kind from the pool.*/
	static ObjectType* ObjectPool<ObjectType>::Allocate()
	{
		if (m_nextAvailable == nullptr)
		{
			if (m_autoExpand > 0)
			{
				addPoolBlock(m_autoExpand);
			}
			else
			{
				return nullptr;
			}
		}

		//Take an object out of the available pool of objects
		ObjectType* newPtr = reinterpret_cast<ObjectType*>(m_nextAvailable);
		m_nextAvailable = m_nextAvailable->m_next;
		
		new (newPtr) ObjectType();
		return newPtr;
	}

	/*Destroys the given object and adds it to the stack of available slots.
	It automatically sets the pointer to a null pointer as an additional safety measure.*/
	static void Free(ObjectType*& _obj)
	{
		_obj->~ObjectType();
		//Convert the object back into a "unused" pointer item
		PoolItem* itemPtr = reinterpret_cast<PoolItem *>(_obj);
		itemPtr->m_next = m_nextAvailable;
		//This is our new next available object slot
		m_nextAvailable = itemPtr;
		_obj = nullptr;
	}

	virtual ~ObjectPool()
	{
		for (std::vector<PoolBlock>::iterator currentBlock = m_blockVector.begin(); currentBlock != m_blockVector.end(); ++currentBlock)
		{
			free( (*currentBlock).m_poolContent );
		}
	}
};

//Static member definitions
template <typename ObjectType>
typename ObjectPool<ObjectType>::PoolItem* ObjectPool<ObjectType>::m_nextAvailable;

template <typename ObjectType>
ObjectType ObjectPool<ObjectType>::m_prototype;

template<typename ObjectType>
size_t ObjectPool<ObjectType>::m_autoExpand = 0;

template<typename ObjectType>
std::vector<typename ObjectPool<ObjectType>::PoolBlock> ObjectPool<ObjectType>::m_blockVector;